<?php include('partials/header.php'); ?> <!--  Header section -->

<!-- Start top Navber Section -->
<?php include('partials/top-nav.php') ?>
<!-- End top Navber Section -->

<!-- Start navber section -->
<?php include('partials/navber.php') ?>
<!-- End navber section -->

<!-- Start slider section -->
	
<!-- End slider section -->


<section>
	<div class="container">
		<?php 
			session_start();
			if(isset($_SESSION["mas"])){
				echo $_SESSION["mas"];
			}
			session_destroy();
		 ?>
	</div>
</section>


<!-- Start Registration Form Section -->

<section>
	<div class="container">
		<h1>Registration Form</h1>
		<div class="row">
			<div class="col-md-6">
				<form action="myPhp/save.php" method="POST">
				  <div class="form-group">
				    <label for="fname">First Name</label>
				    <input type="text" class="form-control" id="fname" placeholder="First Name" name="fname" >
				  </div>
				  <div class="form-group">
				    <label for="lname">Last Name</label>
				    <input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname" >
				  </div>
				  <div class="form-group">
				    <label for="email">E-mail</label>
				    <input type="email" class="form-control" id="email" placeholder="E-mail" name="email" >
				  </div>

				  <button type="Submit" class="btn btn-outline-info btn-lg btn-block">Submit</button>
				</form>
			</div>
			<div class="col-md-6"></div>
		</div>
	</div>
</section>
<!-- End Registration Form Section -->

<?php include('partials/footer.php'); ?><!--  Footer section -->